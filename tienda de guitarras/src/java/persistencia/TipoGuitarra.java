/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Angel
 */
@Entity
@Table(name = "tipo_guitarra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoGuitarra.findAll", query = "SELECT t FROM TipoGuitarra t")
    , @NamedQuery(name = "TipoGuitarra.findByIdTipoGuitarra", query = "SELECT t FROM TipoGuitarra t WHERE t.idTipoGuitarra = :idTipoGuitarra")
    , @NamedQuery(name = "TipoGuitarra.findByTipoGuitarra", query = "SELECT t FROM TipoGuitarra t WHERE t.tipoGuitarra = :tipoGuitarra")})
public class TipoGuitarra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_TIPO_GUITARRA")
    private Integer idTipoGuitarra;
    @Column(name = "TIPO_GUITARRA")
    private String tipoGuitarra;
    @OneToMany(mappedBy = "tipoDeGuitarra")
    private Collection<Venta> ventaCollection;

    public TipoGuitarra() {
    }

    public TipoGuitarra(Integer idTipoGuitarra) {
        this.idTipoGuitarra = idTipoGuitarra;
    }

    public Integer getIdTipoGuitarra() {
        return idTipoGuitarra;
    }

    public void setIdTipoGuitarra(Integer idTipoGuitarra) {
        this.idTipoGuitarra = idTipoGuitarra;
    }

    public String getTipoGuitarra() {
        return tipoGuitarra;
    }

    public void setTipoGuitarra(String tipoGuitarra) {
        this.tipoGuitarra = tipoGuitarra;
    }

    @XmlTransient
    public Collection<Venta> getVentaCollection() {
        return ventaCollection;
    }

    public void setVentaCollection(Collection<Venta> ventaCollection) {
        this.ventaCollection = ventaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoGuitarra != null ? idTipoGuitarra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoGuitarra)) {
            return false;
        }
        TipoGuitarra other = (TipoGuitarra) object;
        if ((this.idTipoGuitarra == null && other.idTipoGuitarra != null) || (this.idTipoGuitarra != null && !this.idTipoGuitarra.equals(other.idTipoGuitarra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TipoGuitarra[ idTipoGuitarra=" + idTipoGuitarra + " ]";
    }
    
}
