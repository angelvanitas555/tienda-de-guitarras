/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Angel
 */
@Entity
@Table(name = "tipo_cuerdas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoCuerdas.findAll", query = "SELECT t FROM TipoCuerdas t")
    , @NamedQuery(name = "TipoCuerdas.findByIdCuerdas", query = "SELECT t FROM TipoCuerdas t WHERE t.idCuerdas = :idCuerdas")
    , @NamedQuery(name = "TipoCuerdas.findByTipoCuerda", query = "SELECT t FROM TipoCuerdas t WHERE t.tipoCuerda = :tipoCuerda")})
public class TipoCuerdas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_CUERDAS")
    private Integer idCuerdas;
    @Column(name = "TIPO_CUERDA")
    private String tipoCuerda;
    @OneToMany(mappedBy = "tipoCuerdas")
    private Collection<Venta> ventaCollection;

    public TipoCuerdas() {
    }

    public TipoCuerdas(Integer idCuerdas) {
        this.idCuerdas = idCuerdas;
    }

    public Integer getIdCuerdas() {
        return idCuerdas;
    }

    public void setIdCuerdas(Integer idCuerdas) {
        this.idCuerdas = idCuerdas;
    }

    public String getTipoCuerda() {
        return tipoCuerda;
    }

    public void setTipoCuerda(String tipoCuerda) {
        this.tipoCuerda = tipoCuerda;
    }

    @XmlTransient
    public Collection<Venta> getVentaCollection() {
        return ventaCollection;
    }

    public void setVentaCollection(Collection<Venta> ventaCollection) {
        this.ventaCollection = ventaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCuerdas != null ? idCuerdas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoCuerdas)) {
            return false;
        }
        TipoCuerdas other = (TipoCuerdas) object;
        if ((this.idCuerdas == null && other.idCuerdas != null) || (this.idCuerdas != null && !this.idCuerdas.equals(other.idCuerdas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TipoCuerdas[ idCuerdas=" + idCuerdas + " ]";
    }
    
}
