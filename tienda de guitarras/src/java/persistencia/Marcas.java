/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Angel
 */
@Entity
@Table(name = "marcas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Marcas.findAll", query = "SELECT m FROM Marcas m")
    , @NamedQuery(name = "Marcas.findByIdMarcasGuitarras", query = "SELECT m FROM Marcas m WHERE m.idMarcasGuitarras = :idMarcasGuitarras")
    , @NamedQuery(name = "Marcas.findByNombreMarca", query = "SELECT m FROM Marcas m WHERE m.nombreMarca = :nombreMarca")})
public class Marcas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_MARCAS_GUITARRAS")
    private Integer idMarcasGuitarras;
    @Column(name = "NOMBRE_MARCA")
    private String nombreMarca;
    @OneToMany(mappedBy = "marcaGuitarra")
    private Collection<Venta> ventaCollection;

    public Marcas() {
    }

    public Marcas(Integer idMarcasGuitarras) {
        this.idMarcasGuitarras = idMarcasGuitarras;
    }

    public Integer getIdMarcasGuitarras() {
        return idMarcasGuitarras;
    }

    public void setIdMarcasGuitarras(Integer idMarcasGuitarras) {
        this.idMarcasGuitarras = idMarcasGuitarras;
    }

    public String getNombreMarca() {
        return nombreMarca;
    }

    public void setNombreMarca(String nombreMarca) {
        this.nombreMarca = nombreMarca;
    }

    @XmlTransient
    public Collection<Venta> getVentaCollection() {
        return ventaCollection;
    }

    public void setVentaCollection(Collection<Venta> ventaCollection) {
        this.ventaCollection = ventaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMarcasGuitarras != null ? idMarcasGuitarras.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Marcas)) {
            return false;
        }
        Marcas other = (Marcas) object;
        if ((this.idMarcasGuitarras == null && other.idMarcasGuitarras != null) || (this.idMarcasGuitarras != null && !this.idMarcasGuitarras.equals(other.idMarcasGuitarras))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.Marcas[ idMarcasGuitarras=" + idMarcasGuitarras + " ]";
    }
    
}
