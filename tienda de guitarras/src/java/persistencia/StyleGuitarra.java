/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Angel
 */
@Entity
@Table(name = "style_guitarra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StyleGuitarra.findAll", query = "SELECT s FROM StyleGuitarra s")
    , @NamedQuery(name = "StyleGuitarra.findByIdStyleGuitarra", query = "SELECT s FROM StyleGuitarra s WHERE s.idStyleGuitarra = :idStyleGuitarra")
    , @NamedQuery(name = "StyleGuitarra.findByStyleGuitarra", query = "SELECT s FROM StyleGuitarra s WHERE s.styleGuitarra = :styleGuitarra")})
public class StyleGuitarra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_STYLE_GUITARRA")
    private Integer idStyleGuitarra;
    @Column(name = "STYLE_GUITARRA")
    private String styleGuitarra;
    @OneToMany(mappedBy = "styleGuitarra")
    private Collection<Venta> ventaCollection;

    public StyleGuitarra() {
    }

    public StyleGuitarra(Integer idStyleGuitarra) {
        this.idStyleGuitarra = idStyleGuitarra;
    }

    public Integer getIdStyleGuitarra() {
        return idStyleGuitarra;
    }

    public void setIdStyleGuitarra(Integer idStyleGuitarra) {
        this.idStyleGuitarra = idStyleGuitarra;
    }

    public String getStyleGuitarra() {
        return styleGuitarra;
    }

    public void setStyleGuitarra(String styleGuitarra) {
        this.styleGuitarra = styleGuitarra;
    }

    @XmlTransient
    public Collection<Venta> getVentaCollection() {
        return ventaCollection;
    }

    public void setVentaCollection(Collection<Venta> ventaCollection) {
        this.ventaCollection = ventaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStyleGuitarra != null ? idStyleGuitarra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StyleGuitarra)) {
            return false;
        }
        StyleGuitarra other = (StyleGuitarra) object;
        if ((this.idStyleGuitarra == null && other.idStyleGuitarra != null) || (this.idStyleGuitarra != null && !this.idStyleGuitarra.equals(other.idStyleGuitarra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.StyleGuitarra[ idStyleGuitarra=" + idStyleGuitarra + " ]";
    }
    
}
