/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Angel
 */
@Entity
@Table(name = "venta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Venta.findAll", query = "SELECT v FROM Venta v")
    , @NamedQuery(name = "Venta.findByIdVenta", query = "SELECT v FROM Venta v WHERE v.idVenta = :idVenta")})
public class Venta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_VENTA")
    private Integer idVenta;
    @JoinColumn(name = "MODELO_GUITARRA", referencedColumnName = "ID_MODELO_GUITARRA")
    @ManyToOne
    private ModelosGuitarras modeloGuitarra;
    @JoinColumn(name = "TIPO_DE_GUITARRA", referencedColumnName = "ID_TIPO_GUITARRA")
    @ManyToOne
    private TipoGuitarra tipoDeGuitarra;
    @JoinColumn(name = "MARCA_GUITARRA", referencedColumnName = "ID_MARCAS_GUITARRAS")
    @ManyToOne
    private Marcas marcaGuitarra;
    @JoinColumn(name = "STYLE_GUITARRA", referencedColumnName = "ID_STYLE_GUITARRA")
    @ManyToOne
    private StyleGuitarra styleGuitarra;
    @JoinColumn(name = "TIPO_CUERDAS", referencedColumnName = "ID_CUERDAS")
    @ManyToOne
    private TipoCuerdas tipoCuerdas;

    public Venta() {
    }

    public Venta(Integer idVenta) {
        this.idVenta = idVenta;
    }

    public Integer getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Integer idVenta) {
        this.idVenta = idVenta;
    }

    public ModelosGuitarras getModeloGuitarra() {
        return modeloGuitarra;
    }

    public void setModeloGuitarra(ModelosGuitarras modeloGuitarra) {
        this.modeloGuitarra = modeloGuitarra;
    }

    public TipoGuitarra getTipoDeGuitarra() {
        return tipoDeGuitarra;
    }

    public void setTipoDeGuitarra(TipoGuitarra tipoDeGuitarra) {
        this.tipoDeGuitarra = tipoDeGuitarra;
    }

    public Marcas getMarcaGuitarra() {
        return marcaGuitarra;
    }

    public void setMarcaGuitarra(Marcas marcaGuitarra) {
        this.marcaGuitarra = marcaGuitarra;
    }

    public StyleGuitarra getStyleGuitarra() {
        return styleGuitarra;
    }

    public void setStyleGuitarra(StyleGuitarra styleGuitarra) {
        this.styleGuitarra = styleGuitarra;
    }

    public TipoCuerdas getTipoCuerdas() {
        return tipoCuerdas;
    }

    public void setTipoCuerdas(TipoCuerdas tipoCuerdas) {
        this.tipoCuerdas = tipoCuerdas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVenta != null ? idVenta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Venta)) {
            return false;
        }
        Venta other = (Venta) object;
        if ((this.idVenta == null && other.idVenta != null) || (this.idVenta != null && !this.idVenta.equals(other.idVenta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.Venta[ idVenta=" + idVenta + " ]";
    }
    
}
