/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Angel
 */
@Entity
@Table(name = "modelos_guitarras")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ModelosGuitarras.findAll", query = "SELECT m FROM ModelosGuitarras m")
    , @NamedQuery(name = "ModelosGuitarras.findByIdModeloGuitarra", query = "SELECT m FROM ModelosGuitarras m WHERE m.idModeloGuitarra = :idModeloGuitarra")
    , @NamedQuery(name = "ModelosGuitarras.findByModeloGuitarra", query = "SELECT m FROM ModelosGuitarras m WHERE m.modeloGuitarra = :modeloGuitarra")})
public class ModelosGuitarras implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_MODELO_GUITARRA")
    private Integer idModeloGuitarra;
    @Column(name = "MODELO_GUITARRA")
    private String modeloGuitarra;
    @OneToMany(mappedBy = "modeloGuitarra")
    private Collection<Venta> ventaCollection;

    public ModelosGuitarras() {
    }

    public ModelosGuitarras(Integer idModeloGuitarra) {
        this.idModeloGuitarra = idModeloGuitarra;
    }

    public Integer getIdModeloGuitarra() {
        return idModeloGuitarra;
    }

    public void setIdModeloGuitarra(Integer idModeloGuitarra) {
        this.idModeloGuitarra = idModeloGuitarra;
    }

    public String getModeloGuitarra() {
        return modeloGuitarra;
    }

    public void setModeloGuitarra(String modeloGuitarra) {
        this.modeloGuitarra = modeloGuitarra;
    }

    @XmlTransient
    public Collection<Venta> getVentaCollection() {
        return ventaCollection;
    }

    public void setVentaCollection(Collection<Venta> ventaCollection) {
        this.ventaCollection = ventaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idModeloGuitarra != null ? idModeloGuitarra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModelosGuitarras)) {
            return false;
        }
        ModelosGuitarras other = (ModelosGuitarras) object;
        if ((this.idModeloGuitarra == null && other.idModeloGuitarra != null) || (this.idModeloGuitarra != null && !this.idModeloGuitarra.equals(other.idModeloGuitarra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.ModelosGuitarras[ idModeloGuitarra=" + idModeloGuitarra + " ]";
    }
    
}
