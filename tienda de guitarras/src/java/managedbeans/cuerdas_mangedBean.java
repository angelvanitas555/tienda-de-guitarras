/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbeans;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import mantenimientos.TipoCuerdasJpaController;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import persistencia.TipoCuerdas;

/**
 *
 * @author Angel
 */
@ManagedBean(name = "cuerdas")
@RequestScoped
public class cuerdas_mangedBean {

    private TipoCuerdas cuerdas = new TipoCuerdas();
    private TipoCuerdasJpaController control = new TipoCuerdasJpaController();

    public TipoCuerdas getCuerdas() {
        return cuerdas;
    }

    public void setCuerdas(TipoCuerdas cuerdas) {
        this.cuerdas = cuerdas;
    }

    public TipoCuerdasJpaController getControl() {
        return control;
    }

    public void setControl(TipoCuerdasJpaController control) {
        this.control = control;
    }

    public cuerdas_mangedBean() {
    }

    public void reporte_cuerdas() throws JRException, IOException {
        
        int idCuerda = cuerdas.getIdCuerdas();
        try {
            JasperReport reporte = (JasperReport) JRLoader.loadObject("cuerdas_reporte.jasper");
            Map parametro = new HashMap();
            parametro.put("idcuerda", idCuerda);

            JasperPrint print = JasperFillManager.fillReport(reporte, parametro);
            JasperViewer reporteview = new JasperViewer(print, false);
        } catch (Exception e) {
            System.err.println("Hay un ERROR en la Generación del REPORTE");
        }
    }

}
