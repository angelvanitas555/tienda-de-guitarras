/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import mantenimientos.TipoGuitarraJpaController;
import persistencia.TipoGuitarra;

/**
 *
 * @author Angel
 */
@ManagedBean(name = "tipe")
@RequestScoped
public class TipoGuitarrasManagedBean {
    TipoGuitarra guitar_type = new TipoGuitarra();
    TipoGuitarraJpaController control = new TipoGuitarraJpaController();

    public TipoGuitarra getGuitar_type() {
        return guitar_type;
    }

    public void setGuitar_type(TipoGuitarra guitar_type) {
        this.guitar_type = guitar_type;
    }

    public TipoGuitarraJpaController getControl() {
        return control;
    }

    public void setControl(TipoGuitarraJpaController control) {
        this.control = control;
    }
    
    public TipoGuitarrasManagedBean() {
    }
    
}
