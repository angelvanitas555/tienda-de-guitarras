/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import mantenimientos.StyleGuitarraJpaController;
import persistencia.StyleGuitarra;

/**
 *
 * @author Angel
 */
@ManagedBean(name = "stylo")
@RequestScoped
public class StyleGuitarra_managedBean {

    StyleGuitarra stylo = new StyleGuitarra();
    StyleGuitarraJpaController control = new StyleGuitarraJpaController();

    public StyleGuitarra getStylo() {
        return stylo;
    }

    public void setStylo(StyleGuitarra stylo) {
        this.stylo = stylo;
    }

    public StyleGuitarraJpaController getControl() {
        return control;
    }

    public void setControl(StyleGuitarraJpaController control) {
        this.control = control;
    }
    
    public StyleGuitarra_managedBean() {
    }
    
}
