/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import mantenimientos.MarcasJpaController;
import mantenimientos.ModelosGuitarrasJpaController;
import mantenimientos.StyleGuitarraJpaController;
import mantenimientos.TipoCuerdasJpaController;
import mantenimientos.TipoGuitarraJpaController;
import mantenimientos.VentaJpaController;
import persistencia.Marcas;
import persistencia.ModelosGuitarras;
import persistencia.StyleGuitarra;
import persistencia.TipoCuerdas;
import persistencia.TipoGuitarra;
import persistencia.Venta;

/**
 *
 * @author Angel
 */
@ManagedBean(name = "sale")
@RequestScoped
public class Venta_mangedbean {

    private Venta sale = new Venta();
    private VentaJpaController control = new VentaJpaController();
    //CLASES DE LAS TABLAS RELACIONADAS A ESTA TABLA DE VENTAS
    private Marcas mark = new Marcas();
    private MarcasJpaController mark_control = new MarcasJpaController();
    private ModelosGuitarras models = new ModelosGuitarras();
    private ModelosGuitarrasJpaController models_control = new ModelosGuitarrasJpaController();
    private StyleGuitarra stylo = new StyleGuitarra();
    private StyleGuitarraJpaController stylo_control = new StyleGuitarraJpaController();
    private TipoCuerdas cuerda = new TipoCuerdas();
    private TipoCuerdasJpaController cuerda_control = new TipoCuerdasJpaController();
    private TipoGuitarra guitype = new TipoGuitarra();
    private TipoGuitarraJpaController guitype_control = new TipoGuitarraJpaController();

    //variables que usaremos de id contendores para traer los id de las otras tablas a estos
    private Integer id_helperMARCAS;
    private Integer id_helperMODELOS;
    private Integer id_helperSTYLOS;
    private Integer id_helperCUERDAS;
    private Integer id_helperTYPEG;

    public Venta getSale() {
        return sale;
    }

    public void setSale(Venta sale) {
        this.sale = sale;
    }

    public VentaJpaController getControl() {
        return control;
    }

    public void setControl(VentaJpaController control) {
        this.control = control;
    }

    public Marcas getMark() {
        return mark;
    }

    public void setMark(Marcas mark) {
        this.mark = mark;
    }

    public MarcasJpaController getMark_control() {
        return mark_control;
    }

    public void setMark_control(MarcasJpaController mark_control) {
        this.mark_control = mark_control;
    }

    public ModelosGuitarras getModels() {
        return models;
    }

    public void setModels(ModelosGuitarras models) {
        this.models = models;
    }

    public ModelosGuitarrasJpaController getModels_control() {
        return models_control;
    }

    public void setModels_control(ModelosGuitarrasJpaController models_control) {
        this.models_control = models_control;
    }

    public StyleGuitarra getStylo() {
        return stylo;
    }

    public void setStylo(StyleGuitarra stylo) {
        this.stylo = stylo;
    }

    public StyleGuitarraJpaController getStylo_control() {
        return stylo_control;
    }

    public void setStylo_control(StyleGuitarraJpaController stylo_control) {
        this.stylo_control = stylo_control;
    }

    public TipoCuerdas getCuerda() {
        return cuerda;
    }

    public void setCuerda(TipoCuerdas cuerda) {
        this.cuerda = cuerda;
    }

    public TipoCuerdasJpaController getCuerda_control() {
        return cuerda_control;
    }

    public void setCuerda_control(TipoCuerdasJpaController cuerda_control) {
        this.cuerda_control = cuerda_control;
    }

    public TipoGuitarra getGuitype() {
        return guitype;
    }

    public void setGuitype(TipoGuitarra guitype) {
        this.guitype = guitype;
    }

    public TipoGuitarraJpaController getGuitype_control() {
        return guitype_control;
    }

    public void setGuitype_control(TipoGuitarraJpaController guitype_control) {
        this.guitype_control = guitype_control;
    }

    public Integer getId_helperMARCAS() {
        return id_helperMARCAS;
    }

    public void setId_helperMARCAS(Integer id_helperMARCAS) {
        this.id_helperMARCAS = id_helperMARCAS;
    }

    public Integer getId_helperMODELOS() {
        return id_helperMODELOS;
    }

    public void setId_helperMODELOS(Integer id_helperMODELOS) {
        this.id_helperMODELOS = id_helperMODELOS;
    }

    public Integer getId_helperSTYLOS() {
        return id_helperSTYLOS;
    }

    public void setId_helperSTYLOS(Integer id_helperSTYLOS) {
        this.id_helperSTYLOS = id_helperSTYLOS;
    }

    public Integer getId_helperCUERDAS() {
        return id_helperCUERDAS;
    }

    public void setId_helperCUERDAS(Integer id_helperCUERDAS) {
        this.id_helperCUERDAS = id_helperCUERDAS;
    }

    public Integer getId_helperTYPEG() {
        return id_helperTYPEG;
    }

    public void setId_helperTYPEG(Integer id_helperTYPEG) {
        this.id_helperTYPEG = id_helperTYPEG;
    }

    public Venta_mangedbean() {
    }

    public void guardar_relacionAll() {
        Venta sale = new Venta();
        mark.setIdMarcasGuitarras(id_helperMARCAS);
        sale.setMarcaGuitarra(mark);
        //control.create(sale);

        models.setIdModeloGuitarra(id_helperMODELOS);
        sale.setModeloGuitarra(models);
        // control.create(sale);

        stylo.setIdStyleGuitarra(id_helperSTYLOS);
        sale.setStyleGuitarra(stylo);
        // control.create(sale);

        cuerda.setIdCuerdas(id_helperCUERDAS);
        sale.setTipoCuerdas(cuerda);
        //control.create(sale);

        guitype.setIdTipoGuitarra(id_helperTYPEG);
        sale.setTipoDeGuitarra(guitype);
        control.create(sale);
    }
}
