/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbeans;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import persistencia.Marcas;
import persistencia.ModelosGuitarras;
import persistencia.StyleGuitarra;
import persistencia.TipoCuerdas;
import persistencia.TipoGuitarra;
import persistencia.Venta;

/**
 *
 * @author Angel
 */
@ManagedBean
@RequestScoped
public class ReporteManagedBean {

   private Marcas marca = new Marcas();
   private ModelosGuitarras model = new  ModelosGuitarras();
   private List<Venta> ventas = new ArrayList<>();
   
   private StyleGuitarra stylo = new StyleGuitarra();
   private TipoCuerdas cuerda = new TipoCuerdas();
   private TipoGuitarra guitar = new TipoGuitarra();
   
    public List<Venta> getVentas() {
        Venta ventass = new Venta();
        ventass.setIdVenta(Integer.SIZE);
        ventass.setMarcaGuitarra(marca);
        ventass.setModeloGuitarra(model);
        ventass.setStyleGuitarra(stylo);
        ventass.setTipoCuerdas(cuerda);
        ventass.setTipoDeGuitarra(guitar);
        ventas.add(ventass);
        return ventas;
    }

    public void setVentas(List<Venta> ventas) {
        this.ventas = ventas;
    }
   
    public void exportar_PDF(){
    
    }
    
    public ReporteManagedBean() {
    }
    
}
