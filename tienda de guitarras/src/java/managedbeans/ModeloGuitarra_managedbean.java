/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import mantenimientos.ModelosGuitarrasJpaController;
import persistencia.ModelosGuitarras;

/**
 *
 * @author Angel
 */
@ManagedBean(name = "models")
@RequestScoped
public class ModeloGuitarra_managedbean {

    ModelosGuitarras modelos = new ModelosGuitarras();
    ModelosGuitarrasJpaController control = new ModelosGuitarrasJpaController();

    public ModelosGuitarras getModelos() {
        return modelos;
    }

    public void setModelos(ModelosGuitarras modelos) {
        this.modelos = modelos;
    }

    public ModelosGuitarrasJpaController getControl() {
        return control;
    }

    public void setControl(ModelosGuitarrasJpaController control) {
        this.control = control;
    }
    
    public ModeloGuitarra_managedbean() {
    }
    
}
