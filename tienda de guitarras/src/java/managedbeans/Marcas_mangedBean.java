/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import mantenimientos.MarcasJpaController;
import persistencia.Marcas;

/**
 *
 * @author Angel
 */
@ManagedBean(name = "mark")
@RequestScoped
public class Marcas_mangedBean {

    Marcas marc = new Marcas();
    MarcasJpaController control = new MarcasJpaController();

    public Marcas getMarc() {
        return marc;
    }

    public void setMarc(Marcas marc) {
        this.marc = marc;
    }

    public MarcasJpaController getControl() {
        return control;
    }

    public void setControl(MarcasJpaController control) {
        this.control = control;
    }
    
    public Marcas_mangedBean() {
    }
    
}
