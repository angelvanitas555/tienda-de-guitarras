/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import mantenimientos.CuerdasMantenimientoMio;
import persistencia.TipoCuerdas;

/**
 *
 * @author Angel
 */
@ManagedBean(name = "striings")
@RequestScoped
public class cuerdas_mio_manejador {

    CuerdasMantenimientoMio mante = new CuerdasMantenimientoMio();
    TipoCuerdas cuerdas = new TipoCuerdas();

    public CuerdasMantenimientoMio getMante() {
        return mante;
    }

    public void setMante(CuerdasMantenimientoMio mante) {
        this.mante = mante;
    }

    public TipoCuerdas getCuerdas() {
        return cuerdas;
    }

    public void setCuerdas(TipoCuerdas cuerdas) {
        this.cuerdas = cuerdas;
    }
    
    public cuerdas_mio_manejador() {
    }
    
}
