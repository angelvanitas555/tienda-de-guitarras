/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimientos;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.Venta;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mantenimientos.exceptions.NonexistentEntityException;
import persistencia.ModelosGuitarras;

/**
 *
 * @author Angel
 */
public class ModelosGuitarrasJpaController implements Serializable {
private EntityManagerFactory emf = null;
    public ModelosGuitarrasJpaController() {
        this.emf = Persistence.createEntityManagerFactory("tienda_de_guitarrasPU");
    }
    

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ModelosGuitarras modelosGuitarras) {
        if (modelosGuitarras.getVentaCollection() == null) {
            modelosGuitarras.setVentaCollection(new ArrayList<Venta>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Venta> attachedVentaCollection = new ArrayList<Venta>();
            for (Venta ventaCollectionVentaToAttach : modelosGuitarras.getVentaCollection()) {
                ventaCollectionVentaToAttach = em.getReference(ventaCollectionVentaToAttach.getClass(), ventaCollectionVentaToAttach.getIdVenta());
                attachedVentaCollection.add(ventaCollectionVentaToAttach);
            }
            modelosGuitarras.setVentaCollection(attachedVentaCollection);
            em.persist(modelosGuitarras);
            for (Venta ventaCollectionVenta : modelosGuitarras.getVentaCollection()) {
                ModelosGuitarras oldModeloGuitarraOfVentaCollectionVenta = ventaCollectionVenta.getModeloGuitarra();
                ventaCollectionVenta.setModeloGuitarra(modelosGuitarras);
                ventaCollectionVenta = em.merge(ventaCollectionVenta);
                if (oldModeloGuitarraOfVentaCollectionVenta != null) {
                    oldModeloGuitarraOfVentaCollectionVenta.getVentaCollection().remove(ventaCollectionVenta);
                    oldModeloGuitarraOfVentaCollectionVenta = em.merge(oldModeloGuitarraOfVentaCollectionVenta);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ModelosGuitarras modelosGuitarras) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ModelosGuitarras persistentModelosGuitarras = em.find(ModelosGuitarras.class, modelosGuitarras.getIdModeloGuitarra());
            Collection<Venta> ventaCollectionOld = persistentModelosGuitarras.getVentaCollection();
            Collection<Venta> ventaCollectionNew = modelosGuitarras.getVentaCollection();
            Collection<Venta> attachedVentaCollectionNew = new ArrayList<Venta>();
            for (Venta ventaCollectionNewVentaToAttach : ventaCollectionNew) {
                ventaCollectionNewVentaToAttach = em.getReference(ventaCollectionNewVentaToAttach.getClass(), ventaCollectionNewVentaToAttach.getIdVenta());
                attachedVentaCollectionNew.add(ventaCollectionNewVentaToAttach);
            }
            ventaCollectionNew = attachedVentaCollectionNew;
            modelosGuitarras.setVentaCollection(ventaCollectionNew);
            modelosGuitarras = em.merge(modelosGuitarras);
            for (Venta ventaCollectionOldVenta : ventaCollectionOld) {
                if (!ventaCollectionNew.contains(ventaCollectionOldVenta)) {
                    ventaCollectionOldVenta.setModeloGuitarra(null);
                    ventaCollectionOldVenta = em.merge(ventaCollectionOldVenta);
                }
            }
            for (Venta ventaCollectionNewVenta : ventaCollectionNew) {
                if (!ventaCollectionOld.contains(ventaCollectionNewVenta)) {
                    ModelosGuitarras oldModeloGuitarraOfVentaCollectionNewVenta = ventaCollectionNewVenta.getModeloGuitarra();
                    ventaCollectionNewVenta.setModeloGuitarra(modelosGuitarras);
                    ventaCollectionNewVenta = em.merge(ventaCollectionNewVenta);
                    if (oldModeloGuitarraOfVentaCollectionNewVenta != null && !oldModeloGuitarraOfVentaCollectionNewVenta.equals(modelosGuitarras)) {
                        oldModeloGuitarraOfVentaCollectionNewVenta.getVentaCollection().remove(ventaCollectionNewVenta);
                        oldModeloGuitarraOfVentaCollectionNewVenta = em.merge(oldModeloGuitarraOfVentaCollectionNewVenta);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = modelosGuitarras.getIdModeloGuitarra();
                if (findModelosGuitarras(id) == null) {
                    throw new NonexistentEntityException("The modelosGuitarras with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ModelosGuitarras modelosGuitarras;
            try {
                modelosGuitarras = em.getReference(ModelosGuitarras.class, id);
                modelosGuitarras.getIdModeloGuitarra();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The modelosGuitarras with id " + id + " no longer exists.", enfe);
            }
            Collection<Venta> ventaCollection = modelosGuitarras.getVentaCollection();
            for (Venta ventaCollectionVenta : ventaCollection) {
                ventaCollectionVenta.setModeloGuitarra(null);
                ventaCollectionVenta = em.merge(ventaCollectionVenta);
            }
            em.remove(modelosGuitarras);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ModelosGuitarras> findModelosGuitarrasEntities() {
        return findModelosGuitarrasEntities(true, -1, -1);
    }

    public List<ModelosGuitarras> findModelosGuitarrasEntities(int maxResults, int firstResult) {
        return findModelosGuitarrasEntities(false, maxResults, firstResult);
    }

    private List<ModelosGuitarras> findModelosGuitarrasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ModelosGuitarras.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ModelosGuitarras findModelosGuitarras(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ModelosGuitarras.class, id);
        } finally {
            em.close();
        }
    }

    public int getModelosGuitarrasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ModelosGuitarras> rt = cq.from(ModelosGuitarras.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
