package mantenimientos;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import persistencia.TipoCuerdas;

/**
 *
 * @author Angel
 */
public class CuerdasMantenimientoMio {

    EntityManagerFactory emf = null;

    public CuerdasMantenimientoMio() {
        this.emf = Persistence.createEntityManagerFactory("tienda_de_guitarrasPU");
    }

    //METODO  PARA CREAR UN REGISTRO EN LA BASE DE DATOS 
    public int crear_registro(TipoCuerdas cuerdas) {
        int flag = 0;
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.persist(cuerdas);
            em.getTransaction().commit();
            flag = 1;
        } catch (Exception e) {
            em.getTransaction().rollback();
            System.err.println("Error" + e);
        } finally {
            em.close();
        }
        return flag;
    }

    //METODO PARA VER TODO EN LA LISTA
    public List verTodo() {
        List<TipoCuerdas> lista_cuerdas = null;
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            Query query = em.createQuery("select t from TipoCuerdas t");
            em.getTransaction().commit();
            lista_cuerdas = query.getResultList();
        } catch (Exception e) {
            System.err.println("ERROR: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista_cuerdas;
    }

    //METODO PARA SELECCIONAR POR ID
    public TipoCuerdas verporID(int id_cuerdas) {

        TipoCuerdas cuerda = null;
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            cuerda = em.find(TipoCuerdas.class, id_cuerdas);
            em.getTransaction().commit();
        } catch (Exception e) {
            System.err.println("ERROR: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return cuerda;
    }

    //METODO PARA ACTUALIZAR
    public int Actualizar(TipoCuerdas cuerdas) {
        int flag = 0;
        TipoCuerdas cuerda;
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            cuerda = em.find(TipoCuerdas.class, cuerdas.getIdCuerdas());
            cuerdas.setTipoCuerda(cuerdas.getTipoCuerda());
            em.merge(cuerdas);
            em.getTransaction().commit();
            flag = 1;
        } catch (Exception e) {
            em.getTransaction().rollback();
            flag = 0;
            System.err.println("ERROR: " + e);
        } finally {
            em.close();
        }
        return flag;
    }

    //METODO PARA ELIMINAR UN REGISTRO POR SU ID EN LA BASE DE DATOS DESDE LA TABLA
    public int eliminar(int id) {
        int flag = 0;
        TipoCuerdas cuerda;
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            cuerda = em.find(TipoCuerdas.class, id);
            em.remove(id);// del id se pasa a capturar la excepción
            em.getTransaction().commit();
            flag = 1;
        } catch (Exception e) {
            System.out.println("ERROR: " + e);
            em.getTransaction().rollback();
            flag = 0;
        } finally {
            em.close();
        }
        return flag;
    }
}
