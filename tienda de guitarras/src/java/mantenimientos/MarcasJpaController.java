/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimientos;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.Venta;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mantenimientos.exceptions.NonexistentEntityException;
import persistencia.Marcas;

/**
 *
 * @author Angel
 */
public class MarcasJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public MarcasJpaController() {
        this.emf = Persistence.createEntityManagerFactory("tienda_de_guitarrasPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Marcas marcas) {
        if (marcas.getVentaCollection() == null) {
            marcas.setVentaCollection(new ArrayList<Venta>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Venta> attachedVentaCollection = new ArrayList<Venta>();
            for (Venta ventaCollectionVentaToAttach : marcas.getVentaCollection()) {
                ventaCollectionVentaToAttach = em.getReference(ventaCollectionVentaToAttach.getClass(), ventaCollectionVentaToAttach.getIdVenta());
                attachedVentaCollection.add(ventaCollectionVentaToAttach);
            }
            marcas.setVentaCollection(attachedVentaCollection);
            em.persist(marcas);
            for (Venta ventaCollectionVenta : marcas.getVentaCollection()) {
                Marcas oldMarcaGuitarraOfVentaCollectionVenta = ventaCollectionVenta.getMarcaGuitarra();
                ventaCollectionVenta.setMarcaGuitarra(marcas);
                ventaCollectionVenta = em.merge(ventaCollectionVenta);
                if (oldMarcaGuitarraOfVentaCollectionVenta != null) {
                    oldMarcaGuitarraOfVentaCollectionVenta.getVentaCollection().remove(ventaCollectionVenta);
                    oldMarcaGuitarraOfVentaCollectionVenta = em.merge(oldMarcaGuitarraOfVentaCollectionVenta);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Marcas marcas) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Marcas persistentMarcas = em.find(Marcas.class, marcas.getIdMarcasGuitarras());
            Collection<Venta> ventaCollectionOld = persistentMarcas.getVentaCollection();
            Collection<Venta> ventaCollectionNew = marcas.getVentaCollection();
            Collection<Venta> attachedVentaCollectionNew = new ArrayList<Venta>();
            for (Venta ventaCollectionNewVentaToAttach : ventaCollectionNew) {
                ventaCollectionNewVentaToAttach = em.getReference(ventaCollectionNewVentaToAttach.getClass(), ventaCollectionNewVentaToAttach.getIdVenta());
                attachedVentaCollectionNew.add(ventaCollectionNewVentaToAttach);
            }
            ventaCollectionNew = attachedVentaCollectionNew;
            marcas.setVentaCollection(ventaCollectionNew);
            marcas = em.merge(marcas);
            for (Venta ventaCollectionOldVenta : ventaCollectionOld) {
                if (!ventaCollectionNew.contains(ventaCollectionOldVenta)) {
                    ventaCollectionOldVenta.setMarcaGuitarra(null);
                    ventaCollectionOldVenta = em.merge(ventaCollectionOldVenta);
                }
            }
            for (Venta ventaCollectionNewVenta : ventaCollectionNew) {
                if (!ventaCollectionOld.contains(ventaCollectionNewVenta)) {
                    Marcas oldMarcaGuitarraOfVentaCollectionNewVenta = ventaCollectionNewVenta.getMarcaGuitarra();
                    ventaCollectionNewVenta.setMarcaGuitarra(marcas);
                    ventaCollectionNewVenta = em.merge(ventaCollectionNewVenta);
                    if (oldMarcaGuitarraOfVentaCollectionNewVenta != null && !oldMarcaGuitarraOfVentaCollectionNewVenta.equals(marcas)) {
                        oldMarcaGuitarraOfVentaCollectionNewVenta.getVentaCollection().remove(ventaCollectionNewVenta);
                        oldMarcaGuitarraOfVentaCollectionNewVenta = em.merge(oldMarcaGuitarraOfVentaCollectionNewVenta);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = marcas.getIdMarcasGuitarras();
                if (findMarcas(id) == null) {
                    throw new NonexistentEntityException("The marcas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Marcas marcas;
            try {
                marcas = em.getReference(Marcas.class, id);
                marcas.getIdMarcasGuitarras();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The marcas with id " + id + " no longer exists.", enfe);
            }
            Collection<Venta> ventaCollection = marcas.getVentaCollection();
            for (Venta ventaCollectionVenta : ventaCollection) {
                ventaCollectionVenta.setMarcaGuitarra(null);
                ventaCollectionVenta = em.merge(ventaCollectionVenta);
            }
            em.remove(marcas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Marcas> findMarcasEntities() {
        return findMarcasEntities(true, -1, -1);
    }

    public List<Marcas> findMarcasEntities(int maxResults, int firstResult) {
        return findMarcasEntities(false, maxResults, firstResult);
    }

    private List<Marcas> findMarcasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Marcas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Marcas findMarcas(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Marcas.class, id);
        } finally {
            em.close();
        }
    }

    public int getMarcasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Marcas> rt = cq.from(Marcas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
