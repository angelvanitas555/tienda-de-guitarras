/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimientos;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.Venta;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mantenimientos.exceptions.NonexistentEntityException;
import persistencia.TipoCuerdas;

/**
 *
 * @author Angel
 */
public class TipoCuerdasJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public TipoCuerdasJpaController() {
        this.emf = Persistence.createEntityManagerFactory("tienda_de_guitarrasPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoCuerdas tipoCuerdas) {
        if (tipoCuerdas.getVentaCollection() == null) {
            tipoCuerdas.setVentaCollection(new ArrayList<Venta>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Venta> attachedVentaCollection = new ArrayList<Venta>();
            for (Venta ventaCollectionVentaToAttach : tipoCuerdas.getVentaCollection()) {
                ventaCollectionVentaToAttach = em.getReference(ventaCollectionVentaToAttach.getClass(), ventaCollectionVentaToAttach.getIdVenta());
                attachedVentaCollection.add(ventaCollectionVentaToAttach);
            }
            tipoCuerdas.setVentaCollection(attachedVentaCollection);
            em.persist(tipoCuerdas);
            for (Venta ventaCollectionVenta : tipoCuerdas.getVentaCollection()) {
                TipoCuerdas oldTipoCuerdasOfVentaCollectionVenta = ventaCollectionVenta.getTipoCuerdas();
                ventaCollectionVenta.setTipoCuerdas(tipoCuerdas);
                ventaCollectionVenta = em.merge(ventaCollectionVenta);
                if (oldTipoCuerdasOfVentaCollectionVenta != null) {
                    oldTipoCuerdasOfVentaCollectionVenta.getVentaCollection().remove(ventaCollectionVenta);
                    oldTipoCuerdasOfVentaCollectionVenta = em.merge(oldTipoCuerdasOfVentaCollectionVenta);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoCuerdas tipoCuerdas) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoCuerdas persistentTipoCuerdas = em.find(TipoCuerdas.class, tipoCuerdas.getIdCuerdas());
            Collection<Venta> ventaCollectionOld = persistentTipoCuerdas.getVentaCollection();
            Collection<Venta> ventaCollectionNew = tipoCuerdas.getVentaCollection();
            Collection<Venta> attachedVentaCollectionNew = new ArrayList<Venta>();
            for (Venta ventaCollectionNewVentaToAttach : ventaCollectionNew) {
                ventaCollectionNewVentaToAttach = em.getReference(ventaCollectionNewVentaToAttach.getClass(), ventaCollectionNewVentaToAttach.getIdVenta());
                attachedVentaCollectionNew.add(ventaCollectionNewVentaToAttach);
            }
            ventaCollectionNew = attachedVentaCollectionNew;
            tipoCuerdas.setVentaCollection(ventaCollectionNew);
            tipoCuerdas = em.merge(tipoCuerdas);
            for (Venta ventaCollectionOldVenta : ventaCollectionOld) {
                if (!ventaCollectionNew.contains(ventaCollectionOldVenta)) {
                    ventaCollectionOldVenta.setTipoCuerdas(null);
                    ventaCollectionOldVenta = em.merge(ventaCollectionOldVenta);
                }
            }
            for (Venta ventaCollectionNewVenta : ventaCollectionNew) {
                if (!ventaCollectionOld.contains(ventaCollectionNewVenta)) {
                    TipoCuerdas oldTipoCuerdasOfVentaCollectionNewVenta = ventaCollectionNewVenta.getTipoCuerdas();
                    ventaCollectionNewVenta.setTipoCuerdas(tipoCuerdas);
                    ventaCollectionNewVenta = em.merge(ventaCollectionNewVenta);
                    if (oldTipoCuerdasOfVentaCollectionNewVenta != null && !oldTipoCuerdasOfVentaCollectionNewVenta.equals(tipoCuerdas)) {
                        oldTipoCuerdasOfVentaCollectionNewVenta.getVentaCollection().remove(ventaCollectionNewVenta);
                        oldTipoCuerdasOfVentaCollectionNewVenta = em.merge(oldTipoCuerdasOfVentaCollectionNewVenta);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipoCuerdas.getIdCuerdas();
                if (findTipoCuerdas(id) == null) {
                    throw new NonexistentEntityException("The tipoCuerdas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoCuerdas tipoCuerdas;
            try {
                tipoCuerdas = em.getReference(TipoCuerdas.class, id);
                tipoCuerdas.getIdCuerdas();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoCuerdas with id " + id + " no longer exists.", enfe);
            }
            Collection<Venta> ventaCollection = tipoCuerdas.getVentaCollection();
            for (Venta ventaCollectionVenta : ventaCollection) {
                ventaCollectionVenta.setTipoCuerdas(null);
                ventaCollectionVenta = em.merge(ventaCollectionVenta);
            }
            em.remove(tipoCuerdas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoCuerdas> findTipoCuerdasEntities() {
        return findTipoCuerdasEntities(true, -1, -1);
    }

    public List<TipoCuerdas> findTipoCuerdasEntities(int maxResults, int firstResult) {
        return findTipoCuerdasEntities(false, maxResults, firstResult);
    }

    private List<TipoCuerdas> findTipoCuerdasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoCuerdas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoCuerdas findTipoCuerdas(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoCuerdas.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoCuerdasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoCuerdas> rt = cq.from(TipoCuerdas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
