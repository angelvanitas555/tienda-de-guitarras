/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimientos;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.Venta;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mantenimientos.exceptions.NonexistentEntityException;
import persistencia.TipoGuitarra;

/**
 *
 * @author Angel
 */
public class TipoGuitarraJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public TipoGuitarraJpaController() {
        this.emf = Persistence.createEntityManagerFactory("tienda_de_guitarrasPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoGuitarra tipoGuitarra) {
        if (tipoGuitarra.getVentaCollection() == null) {
            tipoGuitarra.setVentaCollection(new ArrayList<Venta>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Venta> attachedVentaCollection = new ArrayList<Venta>();
            for (Venta ventaCollectionVentaToAttach : tipoGuitarra.getVentaCollection()) {
                ventaCollectionVentaToAttach = em.getReference(ventaCollectionVentaToAttach.getClass(), ventaCollectionVentaToAttach.getIdVenta());
                attachedVentaCollection.add(ventaCollectionVentaToAttach);
            }
            tipoGuitarra.setVentaCollection(attachedVentaCollection);
            em.persist(tipoGuitarra);
            for (Venta ventaCollectionVenta : tipoGuitarra.getVentaCollection()) {
                TipoGuitarra oldTipoDeGuitarraOfVentaCollectionVenta = ventaCollectionVenta.getTipoDeGuitarra();
                ventaCollectionVenta.setTipoDeGuitarra(tipoGuitarra);
                ventaCollectionVenta = em.merge(ventaCollectionVenta);
                if (oldTipoDeGuitarraOfVentaCollectionVenta != null) {
                    oldTipoDeGuitarraOfVentaCollectionVenta.getVentaCollection().remove(ventaCollectionVenta);
                    oldTipoDeGuitarraOfVentaCollectionVenta = em.merge(oldTipoDeGuitarraOfVentaCollectionVenta);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoGuitarra tipoGuitarra) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoGuitarra persistentTipoGuitarra = em.find(TipoGuitarra.class, tipoGuitarra.getIdTipoGuitarra());
            Collection<Venta> ventaCollectionOld = persistentTipoGuitarra.getVentaCollection();
            Collection<Venta> ventaCollectionNew = tipoGuitarra.getVentaCollection();
            Collection<Venta> attachedVentaCollectionNew = new ArrayList<Venta>();
            for (Venta ventaCollectionNewVentaToAttach : ventaCollectionNew) {
                ventaCollectionNewVentaToAttach = em.getReference(ventaCollectionNewVentaToAttach.getClass(), ventaCollectionNewVentaToAttach.getIdVenta());
                attachedVentaCollectionNew.add(ventaCollectionNewVentaToAttach);
            }
            ventaCollectionNew = attachedVentaCollectionNew;
            tipoGuitarra.setVentaCollection(ventaCollectionNew);
            tipoGuitarra = em.merge(tipoGuitarra);
            for (Venta ventaCollectionOldVenta : ventaCollectionOld) {
                if (!ventaCollectionNew.contains(ventaCollectionOldVenta)) {
                    ventaCollectionOldVenta.setTipoDeGuitarra(null);
                    ventaCollectionOldVenta = em.merge(ventaCollectionOldVenta);
                }
            }
            for (Venta ventaCollectionNewVenta : ventaCollectionNew) {
                if (!ventaCollectionOld.contains(ventaCollectionNewVenta)) {
                    TipoGuitarra oldTipoDeGuitarraOfVentaCollectionNewVenta = ventaCollectionNewVenta.getTipoDeGuitarra();
                    ventaCollectionNewVenta.setTipoDeGuitarra(tipoGuitarra);
                    ventaCollectionNewVenta = em.merge(ventaCollectionNewVenta);
                    if (oldTipoDeGuitarraOfVentaCollectionNewVenta != null && !oldTipoDeGuitarraOfVentaCollectionNewVenta.equals(tipoGuitarra)) {
                        oldTipoDeGuitarraOfVentaCollectionNewVenta.getVentaCollection().remove(ventaCollectionNewVenta);
                        oldTipoDeGuitarraOfVentaCollectionNewVenta = em.merge(oldTipoDeGuitarraOfVentaCollectionNewVenta);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipoGuitarra.getIdTipoGuitarra();
                if (findTipoGuitarra(id) == null) {
                    throw new NonexistentEntityException("The tipoGuitarra with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoGuitarra tipoGuitarra;
            try {
                tipoGuitarra = em.getReference(TipoGuitarra.class, id);
                tipoGuitarra.getIdTipoGuitarra();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoGuitarra with id " + id + " no longer exists.", enfe);
            }
            Collection<Venta> ventaCollection = tipoGuitarra.getVentaCollection();
            for (Venta ventaCollectionVenta : ventaCollection) {
                ventaCollectionVenta.setTipoDeGuitarra(null);
                ventaCollectionVenta = em.merge(ventaCollectionVenta);
            }
            em.remove(tipoGuitarra);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoGuitarra> findTipoGuitarraEntities() {
        return findTipoGuitarraEntities(true, -1, -1);
    }

    public List<TipoGuitarra> findTipoGuitarraEntities(int maxResults, int firstResult) {
        return findTipoGuitarraEntities(false, maxResults, firstResult);
    }

    private List<TipoGuitarra> findTipoGuitarraEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoGuitarra.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoGuitarra findTipoGuitarra(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoGuitarra.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoGuitarraCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoGuitarra> rt = cq.from(TipoGuitarra.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
