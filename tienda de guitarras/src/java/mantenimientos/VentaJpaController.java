/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimientos;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import mantenimientos.exceptions.NonexistentEntityException;
import persistencia.ModelosGuitarras;
import persistencia.TipoGuitarra;
import persistencia.Marcas;
import persistencia.StyleGuitarra;
import persistencia.TipoCuerdas;
import persistencia.Venta;

/**
 *
 * @author Angel
 */
public class VentaJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public VentaJpaController() {
        this.emf = Persistence.createEntityManagerFactory("tienda_de_guitarrasPU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Venta venta) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ModelosGuitarras modeloGuitarra = venta.getModeloGuitarra();
            if (modeloGuitarra != null) {
                modeloGuitarra = em.getReference(modeloGuitarra.getClass(), modeloGuitarra.getIdModeloGuitarra());
                venta.setModeloGuitarra(modeloGuitarra);
            }
            TipoGuitarra tipoDeGuitarra = venta.getTipoDeGuitarra();
            if (tipoDeGuitarra != null) {
                tipoDeGuitarra = em.getReference(tipoDeGuitarra.getClass(), tipoDeGuitarra.getIdTipoGuitarra());
                venta.setTipoDeGuitarra(tipoDeGuitarra);
            }
            Marcas marcaGuitarra = venta.getMarcaGuitarra();
            if (marcaGuitarra != null) {
                marcaGuitarra = em.getReference(marcaGuitarra.getClass(), marcaGuitarra.getIdMarcasGuitarras());
                venta.setMarcaGuitarra(marcaGuitarra);
            }
            StyleGuitarra styleGuitarra = venta.getStyleGuitarra();
            if (styleGuitarra != null) {
                styleGuitarra = em.getReference(styleGuitarra.getClass(), styleGuitarra.getIdStyleGuitarra());
                venta.setStyleGuitarra(styleGuitarra);
            }
            TipoCuerdas tipoCuerdas = venta.getTipoCuerdas();
            if (tipoCuerdas != null) {
                tipoCuerdas = em.getReference(tipoCuerdas.getClass(), tipoCuerdas.getIdCuerdas());
                venta.setTipoCuerdas(tipoCuerdas);
            }
            em.persist(venta);
            if (modeloGuitarra != null) {
                modeloGuitarra.getVentaCollection().add(venta);
                modeloGuitarra = em.merge(modeloGuitarra);
            }
            if (tipoDeGuitarra != null) {
                tipoDeGuitarra.getVentaCollection().add(venta);
                tipoDeGuitarra = em.merge(tipoDeGuitarra);
            }
            if (marcaGuitarra != null) {
                marcaGuitarra.getVentaCollection().add(venta);
                marcaGuitarra = em.merge(marcaGuitarra);
            }
            if (styleGuitarra != null) {
                styleGuitarra.getVentaCollection().add(venta);
                styleGuitarra = em.merge(styleGuitarra);
            }
            if (tipoCuerdas != null) {
                tipoCuerdas.getVentaCollection().add(venta);
                tipoCuerdas = em.merge(tipoCuerdas);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Venta venta) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Venta persistentVenta = em.find(Venta.class, venta.getIdVenta());
            ModelosGuitarras modeloGuitarraOld = persistentVenta.getModeloGuitarra();
            ModelosGuitarras modeloGuitarraNew = venta.getModeloGuitarra();
            TipoGuitarra tipoDeGuitarraOld = persistentVenta.getTipoDeGuitarra();
            TipoGuitarra tipoDeGuitarraNew = venta.getTipoDeGuitarra();
            Marcas marcaGuitarraOld = persistentVenta.getMarcaGuitarra();
            Marcas marcaGuitarraNew = venta.getMarcaGuitarra();
            StyleGuitarra styleGuitarraOld = persistentVenta.getStyleGuitarra();
            StyleGuitarra styleGuitarraNew = venta.getStyleGuitarra();
            TipoCuerdas tipoCuerdasOld = persistentVenta.getTipoCuerdas();
            TipoCuerdas tipoCuerdasNew = venta.getTipoCuerdas();
            if (modeloGuitarraNew != null) {
                modeloGuitarraNew = em.getReference(modeloGuitarraNew.getClass(), modeloGuitarraNew.getIdModeloGuitarra());
                venta.setModeloGuitarra(modeloGuitarraNew);
            }
            if (tipoDeGuitarraNew != null) {
                tipoDeGuitarraNew = em.getReference(tipoDeGuitarraNew.getClass(), tipoDeGuitarraNew.getIdTipoGuitarra());
                venta.setTipoDeGuitarra(tipoDeGuitarraNew);
            }
            if (marcaGuitarraNew != null) {
                marcaGuitarraNew = em.getReference(marcaGuitarraNew.getClass(), marcaGuitarraNew.getIdMarcasGuitarras());
                venta.setMarcaGuitarra(marcaGuitarraNew);
            }
            if (styleGuitarraNew != null) {
                styleGuitarraNew = em.getReference(styleGuitarraNew.getClass(), styleGuitarraNew.getIdStyleGuitarra());
                venta.setStyleGuitarra(styleGuitarraNew);
            }
            if (tipoCuerdasNew != null) {
                tipoCuerdasNew = em.getReference(tipoCuerdasNew.getClass(), tipoCuerdasNew.getIdCuerdas());
                venta.setTipoCuerdas(tipoCuerdasNew);
            }
            venta = em.merge(venta);
            if (modeloGuitarraOld != null && !modeloGuitarraOld.equals(modeloGuitarraNew)) {
                modeloGuitarraOld.getVentaCollection().remove(venta);
                modeloGuitarraOld = em.merge(modeloGuitarraOld);
            }
            if (modeloGuitarraNew != null && !modeloGuitarraNew.equals(modeloGuitarraOld)) {
                modeloGuitarraNew.getVentaCollection().add(venta);
                modeloGuitarraNew = em.merge(modeloGuitarraNew);
            }
            if (tipoDeGuitarraOld != null && !tipoDeGuitarraOld.equals(tipoDeGuitarraNew)) {
                tipoDeGuitarraOld.getVentaCollection().remove(venta);
                tipoDeGuitarraOld = em.merge(tipoDeGuitarraOld);
            }
            if (tipoDeGuitarraNew != null && !tipoDeGuitarraNew.equals(tipoDeGuitarraOld)) {
                tipoDeGuitarraNew.getVentaCollection().add(venta);
                tipoDeGuitarraNew = em.merge(tipoDeGuitarraNew);
            }
            if (marcaGuitarraOld != null && !marcaGuitarraOld.equals(marcaGuitarraNew)) {
                marcaGuitarraOld.getVentaCollection().remove(venta);
                marcaGuitarraOld = em.merge(marcaGuitarraOld);
            }
            if (marcaGuitarraNew != null && !marcaGuitarraNew.equals(marcaGuitarraOld)) {
                marcaGuitarraNew.getVentaCollection().add(venta);
                marcaGuitarraNew = em.merge(marcaGuitarraNew);
            }
            if (styleGuitarraOld != null && !styleGuitarraOld.equals(styleGuitarraNew)) {
                styleGuitarraOld.getVentaCollection().remove(venta);
                styleGuitarraOld = em.merge(styleGuitarraOld);
            }
            if (styleGuitarraNew != null && !styleGuitarraNew.equals(styleGuitarraOld)) {
                styleGuitarraNew.getVentaCollection().add(venta);
                styleGuitarraNew = em.merge(styleGuitarraNew);
            }
            if (tipoCuerdasOld != null && !tipoCuerdasOld.equals(tipoCuerdasNew)) {
                tipoCuerdasOld.getVentaCollection().remove(venta);
                tipoCuerdasOld = em.merge(tipoCuerdasOld);
            }
            if (tipoCuerdasNew != null && !tipoCuerdasNew.equals(tipoCuerdasOld)) {
                tipoCuerdasNew.getVentaCollection().add(venta);
                tipoCuerdasNew = em.merge(tipoCuerdasNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = venta.getIdVenta();
                if (findVenta(id) == null) {
                    throw new NonexistentEntityException("The venta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Venta venta;
            try {
                venta = em.getReference(Venta.class, id);
                venta.getIdVenta();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The venta with id " + id + " no longer exists.", enfe);
            }
            ModelosGuitarras modeloGuitarra = venta.getModeloGuitarra();
            if (modeloGuitarra != null) {
                modeloGuitarra.getVentaCollection().remove(venta);
                modeloGuitarra = em.merge(modeloGuitarra);
            }
            TipoGuitarra tipoDeGuitarra = venta.getTipoDeGuitarra();
            if (tipoDeGuitarra != null) {
                tipoDeGuitarra.getVentaCollection().remove(venta);
                tipoDeGuitarra = em.merge(tipoDeGuitarra);
            }
            Marcas marcaGuitarra = venta.getMarcaGuitarra();
            if (marcaGuitarra != null) {
                marcaGuitarra.getVentaCollection().remove(venta);
                marcaGuitarra = em.merge(marcaGuitarra);
            }
            StyleGuitarra styleGuitarra = venta.getStyleGuitarra();
            if (styleGuitarra != null) {
                styleGuitarra.getVentaCollection().remove(venta);
                styleGuitarra = em.merge(styleGuitarra);
            }
            TipoCuerdas tipoCuerdas = venta.getTipoCuerdas();
            if (tipoCuerdas != null) {
                tipoCuerdas.getVentaCollection().remove(venta);
                tipoCuerdas = em.merge(tipoCuerdas);
            }
            em.remove(venta);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Venta> findVentaEntities() {
        return findVentaEntities(true, -1, -1);
    }

    public List<Venta> findVentaEntities(int maxResults, int firstResult) {
        return findVentaEntities(false, maxResults, firstResult);
    }

    private List<Venta> findVentaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Venta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Venta findVenta(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Venta.class, id);
        } finally {
            em.close();
        }
    }

    public int getVentaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Venta> rt = cq.from(Venta.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
