/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimientos;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.Venta;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mantenimientos.exceptions.NonexistentEntityException;
import persistencia.StyleGuitarra;

/**
 *
 * @author Angel
 */
public class StyleGuitarraJpaController implements Serializable {
private EntityManagerFactory emf = null;
    public StyleGuitarraJpaController() {
        this.emf = Persistence.createEntityManagerFactory("tienda_de_guitarrasPU");
    }
    

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(StyleGuitarra styleGuitarra) {
        if (styleGuitarra.getVentaCollection() == null) {
            styleGuitarra.setVentaCollection(new ArrayList<Venta>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Venta> attachedVentaCollection = new ArrayList<Venta>();
            for (Venta ventaCollectionVentaToAttach : styleGuitarra.getVentaCollection()) {
                ventaCollectionVentaToAttach = em.getReference(ventaCollectionVentaToAttach.getClass(), ventaCollectionVentaToAttach.getIdVenta());
                attachedVentaCollection.add(ventaCollectionVentaToAttach);
            }
            styleGuitarra.setVentaCollection(attachedVentaCollection);
            em.persist(styleGuitarra);
            for (Venta ventaCollectionVenta : styleGuitarra.getVentaCollection()) {
                StyleGuitarra oldStyleGuitarraOfVentaCollectionVenta = ventaCollectionVenta.getStyleGuitarra();
                ventaCollectionVenta.setStyleGuitarra(styleGuitarra);
                ventaCollectionVenta = em.merge(ventaCollectionVenta);
                if (oldStyleGuitarraOfVentaCollectionVenta != null) {
                    oldStyleGuitarraOfVentaCollectionVenta.getVentaCollection().remove(ventaCollectionVenta);
                    oldStyleGuitarraOfVentaCollectionVenta = em.merge(oldStyleGuitarraOfVentaCollectionVenta);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(StyleGuitarra styleGuitarra) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StyleGuitarra persistentStyleGuitarra = em.find(StyleGuitarra.class, styleGuitarra.getIdStyleGuitarra());
            Collection<Venta> ventaCollectionOld = persistentStyleGuitarra.getVentaCollection();
            Collection<Venta> ventaCollectionNew = styleGuitarra.getVentaCollection();
            Collection<Venta> attachedVentaCollectionNew = new ArrayList<Venta>();
            for (Venta ventaCollectionNewVentaToAttach : ventaCollectionNew) {
                ventaCollectionNewVentaToAttach = em.getReference(ventaCollectionNewVentaToAttach.getClass(), ventaCollectionNewVentaToAttach.getIdVenta());
                attachedVentaCollectionNew.add(ventaCollectionNewVentaToAttach);
            }
            ventaCollectionNew = attachedVentaCollectionNew;
            styleGuitarra.setVentaCollection(ventaCollectionNew);
            styleGuitarra = em.merge(styleGuitarra);
            for (Venta ventaCollectionOldVenta : ventaCollectionOld) {
                if (!ventaCollectionNew.contains(ventaCollectionOldVenta)) {
                    ventaCollectionOldVenta.setStyleGuitarra(null);
                    ventaCollectionOldVenta = em.merge(ventaCollectionOldVenta);
                }
            }
            for (Venta ventaCollectionNewVenta : ventaCollectionNew) {
                if (!ventaCollectionOld.contains(ventaCollectionNewVenta)) {
                    StyleGuitarra oldStyleGuitarraOfVentaCollectionNewVenta = ventaCollectionNewVenta.getStyleGuitarra();
                    ventaCollectionNewVenta.setStyleGuitarra(styleGuitarra);
                    ventaCollectionNewVenta = em.merge(ventaCollectionNewVenta);
                    if (oldStyleGuitarraOfVentaCollectionNewVenta != null && !oldStyleGuitarraOfVentaCollectionNewVenta.equals(styleGuitarra)) {
                        oldStyleGuitarraOfVentaCollectionNewVenta.getVentaCollection().remove(ventaCollectionNewVenta);
                        oldStyleGuitarraOfVentaCollectionNewVenta = em.merge(oldStyleGuitarraOfVentaCollectionNewVenta);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = styleGuitarra.getIdStyleGuitarra();
                if (findStyleGuitarra(id) == null) {
                    throw new NonexistentEntityException("The styleGuitarra with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StyleGuitarra styleGuitarra;
            try {
                styleGuitarra = em.getReference(StyleGuitarra.class, id);
                styleGuitarra.getIdStyleGuitarra();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The styleGuitarra with id " + id + " no longer exists.", enfe);
            }
            Collection<Venta> ventaCollection = styleGuitarra.getVentaCollection();
            for (Venta ventaCollectionVenta : ventaCollection) {
                ventaCollectionVenta.setStyleGuitarra(null);
                ventaCollectionVenta = em.merge(ventaCollectionVenta);
            }
            em.remove(styleGuitarra);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<StyleGuitarra> findStyleGuitarraEntities() {
        return findStyleGuitarraEntities(true, -1, -1);
    }

    public List<StyleGuitarra> findStyleGuitarraEntities(int maxResults, int firstResult) {
        return findStyleGuitarraEntities(false, maxResults, firstResult);
    }

    private List<StyleGuitarra> findStyleGuitarraEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(StyleGuitarra.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public StyleGuitarra findStyleGuitarra(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(StyleGuitarra.class, id);
        } finally {
            em.close();
        }
    }

    public int getStyleGuitarraCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<StyleGuitarra> rt = cq.from(StyleGuitarra.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
