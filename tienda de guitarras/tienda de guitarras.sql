CREATE DATABASE MUSICA_TIENDA;
USE MUSICA_TIENDA;

CREATE TABLE STYLE_GUITARRA(
ID_STYLE_GUITARRA INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
STYLE_GUITARRA VARCHAR(50) NULL
);

CREATE TABLE MODELOS_GUITARRAS(
ID_MODELO_GUITARRA INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
MODELO_GUITARRA VARCHAR(50) NULL -- AQUÍ IRÁ EL NOMBRE DEL MODELO DE LA GUITARRA Y SE BUSCARÁ POR EL ID CORRESPONDIENTE
);

CREATE TABLE TIPO_CUERDAS(
ID_CUERDAS INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
TIPO_CUERDA VARCHAR(50) NULL -- PUEDEN SER DE METAL O DE NYLON
);

CREATE TABLE TIPO_GUITARRA(
ID_TIPO_GUITARRA INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
TIPO_GUITARRA VARCHAR(40) NULL -- PUEDEN SER ELECTRICAS, ACÚSTICAS 
);

CREATE TABLE MARCAS(
ID_MARCAS_GUITARRAS INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
NOMBRE_MARCA VARCHAR(50) NULL
);

CREATE TABLE VENTA(
ID_VENTA INT NOT NULL auto_increment PRIMARY KEY,
MODELO_GUITARRA INT NULL,
TIPO_DE_GUITARRA INT NULL,
MARCA_GUITARRA INT NULL,
STYLE_GUITARRA INT NULL,
TIPO_CUERDAS INT NULL,
FOREIGN KEY(MODELO_GUITARRA)REFERENCES MODELOS_GUITARRAS (ID_MODELO_GUITARRA),
FOREIGN KEY(TIPO_DE_GUITARRA)REFERENCES TIPO_GUITARRA (ID_TIPO_GUITARRA),
FOREIGN KEY(MARCA_GUITARRA)REFERENCES MARCAS (ID_MARCAS_GUITARRAS),
FOREIGN KEY(STYLE_GUITARRA)REFERENCES STYLE_GUITARRA (ID_STYLE_GUITARRA),
FOREIGN KEY(TIPO_CUERDAS)REFERENCES TIPO_CUERDAS (ID_CUERDAS)
);

